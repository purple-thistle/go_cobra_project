package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	// Use:   "",
	// Short: "Get absolute path",
	// Long:  `A tool that gives you an absolute path of a given file`,
	// Run: func(cmd *cobra.Command, args []string) {
	// },
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
