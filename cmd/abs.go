package cmd

import (
	"fmt"
	"os"
	"path/filepath"

	"github.com/spf13/cobra"
)

var dFlag bool

func init() {
	rootCmd.AddCommand(absCmd)

	absCmd.PersistentFlags().BoolVarP(&dFlag, "d", "d", false, "required flag")
	absCmd.MarkPersistentFlagRequired("d")
}

var absCmd = &cobra.Command{
	Use:   "abs filename",
	Short: "Get absolute path",
	Long:  `A tool that gives you an absolute path of a given file`,
	Args:  cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		getAbs(args[0])
	},
}

func getAbs(filename string) {

	s, err := filepath.Abs(filename)

	if err != nil {
		os.Exit(1)
	}

	fmt.Println(s)
}
