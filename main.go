package main

import (
	"gitlab.com/purple-thistle/go_cobra_project/cmd"
)

func main() {
	cmd.Execute()
}
